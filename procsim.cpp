#include "procsim.hpp"

#include <iostream>
#include <queue>
using namespace std;

// GLOBAL VARIABLES
int cycles = 0;
int total_dq_size = 0;
int total_fired = 0;
int max_dq_size = 0;
int total_fetched = 0;
int total_finished = 0;
int inst_num = 0;
proc_inst_t p_inst;

// Instruction Queue
//queue<proc_inst_t> inst;
int fetch_rate;

// Dispatch Queue
queue<proc_inst_t> disp_q;

// Scoreboard
//bool *scoreboard;

//Register File
struct register_file {
    bool ready;
    uint64_t tag;
};
register_file *rf;

//RS:
struct reservation_station {
    int fu;
    int dest_reg;
    uint64_t dest_tag;
    bool src_ready[2];
    uint64_t src_tag[2];
    bool valid;
    int free_age;
    int inst_num;
};
reservation_station *rs_q;        // Scheduling Queue
int rs_q_size = 0;
int rs_size;
reservation_station *rs;
int *fire;                 //Contains Index of Instructions to be fired.
queue<int> sq;  //Contains index of sq that are used in filling order (lowest dest tag first)
queue<int> fire_q;  //Contains the index of sq that are marked for fire in tag order (lowest dest tag first)

//CDB
struct result_bus {
    bool busy;
    uint64_t tag;
    int reg;
    int inst_num;
};
int num_cdb;
result_bus *cdb;

//FU - Scoreboard
struct functional_unit {
    int reg;
    uint64_t tag;
    bool busy;
    int inst_num;
};
int num_fu;
functional_unit *fu[3];
int fu_num[3];
queue<int> exe_q;
queue<int> del;
//Output
struct output {
    uint64_t fetch_cycle;
    uint64_t dispatch_cycle;
    uint64_t schedule_cycle;
    uint64_t execute_cycle;
    uint64_t state_update_cycle;
};
output op[100000];

/** Subroutine for initializing stats
* @p_stats Pointer to the statistics structure
*/
void init_stats(proc_stats_t *p_stats) {
    p_stats->avg_disp_size = 0;
    p_stats->avg_inst_fired = 0;
    p_stats->avg_disp_size = 0;
    p_stats->max_disp_size = 0;
    p_stats->retired_instruction = 0;
    p_stats->cycle_count = 0;
}


/**
 * Subroutine for initializing the processor. You many add and initialize any global or heap
 * variables as needed.
 * XXX: You're responsible for completing this routine
 *
 * @r ROB size
 * @k0 Number of k0 FUs
 * @k1 Number of k1 FUs
 * @k2 Number of k2 FUs
 * @f Number of instructions to fetch
 */
void setup_proc(uint64_t r, uint64_t k0, uint64_t k1, uint64_t k2, uint64_t f) {

    fetch_rate = f;

    //Register File
    rf = new register_file[128];
    for(int i=0;i<128;i++) {
        rf[i].ready = true;
    }
    //Reservation Station
    rs_size = 2*(k0+k1+k2);
    rs = new reservation_station[rs_size];
    fire = new int[rs_size];
    for(int i=0;i<rs_size;i++) {
        rs[i].valid = false;
        //rs[i].fired = false;
        rs[i].free_age = 0;
    }

    // Function(al) Units
    num_fu = k0+k1+k2;
    fu_num[0] = k0; fu_num[1] = k1; fu_num[2] = k2;
    for(int i=0;i<3;i++) {
        fu[i] = new functional_unit[fu_num[i]];
        for(int j=0;j<fu_num[i];j++) fu[i][j].busy = false;
    }

    //Result Buses/Common data Bus
    num_cdb = r;
    cdb = new result_bus[num_cdb];
    for(int i=0;i<num_cdb;i++) {
        cdb[i].busy = false;
    }
}
/** Subroutine to fetch instructions from the einstruction file
* and Put them in the dispatch queue
* updates fetch_cycle and dispatch_cycle output stats
*/
bool fetch(bool debug) {
    bool cont = true;
    for(int i=0;i<fetch_rate;i++) {
        if(read_instruction( &p_inst) ) {
            //cout << dec << cycles << " Fetched " << dec << inst_num << " " << hex <<p_inst.instruction_address<<endl;
            //Stat: fetch_cycle, dispatch_cycle
            op[inst_num].fetch_cycle = cycles;
            op[inst_num].dispatch_cycle = cycles + 1;
            p_inst.inst_num = inst_num;
            if(p_inst.op_code == -1) p_inst.op_code = 1;
            disp_q.push(p_inst);
            inst_num++;
            total_fetched++;
            if(debug) {
                cout << cycles << " Fetched: " << p_inst.inst_num+1 <<endl;
            }
        }
        else cont = false;
    }
    return cont;
}

/**
 * Subroutine that simulates the processor.
 *   The processor should fetch instructions as appropriate, until all instructions have executed
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void run_proc(proc_stats_t* p_stats) {
    bool debug = false;
    do {
        if(debug) cout << endl;
        cycles++;
        /** First Half Cycle */
            //Print DQ contents
        if(debug){
            cout <<cycles <<endl;
            int temp_dqSize1 = disp_q.size();
            for(int i=0;i<temp_dqSize1;i++) {
                proc_inst_t temp = disp_q.front();
                cout << "\t\t" << temp.inst_num << endl;
                disp_q.pop();
                disp_q.push(temp);
            }
        }

        //Mark Instructions on cdb from prev as completed and delete their content from SQ/RS. Free CDB
        //** Just delete inst on del q from SQ

        while( !del.empty() ) {
            int temp = del.front();
            del.pop();
            rs[temp].valid = false;
            rs[temp].free_age = cycles;
            //rs[temp].fired = false;
            if(debug) cout << cycles << " Freed " << temp <<endl;
        }



        //Execute the instructions marked to execute in (k-1), 1hc: Broadcast values on free cdb, free FU, and update reg file. 2hc: update sq.
        for(int i=0;i<num_cdb;i++) {
            while(!exe_q.empty() && !cdb[i].busy) {
                int temp = exe_q.front();
                int temp_fu = rs[temp].fu;
                //bool executed;
                for(int j=0;j<fu_num[temp_fu];j++) {
                    if(fu[temp_fu][j].busy && fu[temp_fu][j].inst_num == rs[temp].inst_num) {
                        exe_q.pop();
                        del.push(temp);
                        //cout << "Del Q pushed" << temp <<endl;
                        //Free FU
                        fu[temp_fu][j].busy = false;
                        //Put Fu data on free cdb
                        cdb[i].busy = true;
                        cdb[i].inst_num = fu[temp_fu][j].inst_num;
                        cdb[i].reg = fu[temp_fu][j].reg;
                        cdb[i].tag = fu[temp_fu][j].tag;
                        //Update RF
                        if(cdb[i].reg != -1 && cdb[i].tag == rf[cdb[i].reg].tag) {
                            rf[cdb[i].reg].ready = true;
                        }
                        //Stat: state_update_cycle
                        total_fired++;
                        op[cdb[i].inst_num].state_update_cycle = cycles+1; //update output
                        total_finished++;
                        if(debug) {
                            cout << cycles << " State Updated: " << cdb[i].inst_num+1 << '\t' << temp << endl;
                        }
                        break;
                    }
                }
            }
        }


        /**Schedule: 1hc: If source registers are ready and fu is free, mark them for fire*/
        // Add age coubnter for free slot. Update present cycle as age when freed. Update slot only if cycle-free>1
        //Mark for fire
        int sq_size = sq.size();
        for(int i=0;i<sq_size;i++) {
            if(!sq.empty()) {
                int temp = sq.front();
                sq.pop();
                bool fired = false;
                // 9 passes this condition, so it doesn't get put back on the queue:- solved
                if(rs[temp].valid && rs[temp].src_ready[0] && rs[temp].src_ready[1]){
                    int temp_fu = rs[temp].fu;
                    for(int j=0;j<fu_num[temp_fu];j++) {
                        if( !fu[temp_fu][j].busy ) {       // 'j'th fu of type rs[i]
                            fu[temp_fu][j].busy = true;
                            fu[temp_fu][j].inst_num = rs[temp].inst_num;
                            fu[temp_fu][j].reg = rs[temp].dest_reg;
                            fu[temp_fu][j].tag = rs[temp].dest_tag;
                            exe_q.push(temp);
                            //Stat: execute_cycle
                            op[rs[temp].inst_num].execute_cycle = cycles + 1; //update output
                            if(debug) {
                                cout << cycles << " Executed: " << rs[temp].inst_num+1 <<endl;
                            }
                            fired = true;
                            break;
                        }
                    }
                    if(!fired) sq.push(temp);
                }
                else {
                    sq.push(temp);
                }
            }
            else {
            cout << "!!!!!!!EMPTY!!!!!!!!!"<<endl;}
        }
        //Update RS values from cdb
        for(int i=0;i<num_cdb;i++) {
            if(cdb[i].busy) {
                for(int j=0;j<rs_size;j++) {
                    if(rs[j].valid){//} && !rs[j].fired) {
                        if(!rs[j].src_ready[0] && rs[j].src_tag[0] == cdb[i].tag) {
                            rs[j].src_ready[0] = true;
                        }
                        if(!rs[j].src_ready[1] && rs[j].src_tag[1] == cdb[i].tag) {
                            rs[j].src_ready[1] = true;
                        }
                    }
                }
                cdb[i].busy = false; //Reset CDB
            }
        }


        /**Dispatch: Put data from disq in free slots of SQ*/

        for(int i=0; i<rs_size;i++) {
            if(!rs[i].valid && !disp_q.empty() && cycles > rs[i].free_age) {  //space is available in RS
                proc_inst_t inst = disp_q.front();
                disp_q.pop();

                sq.push(i);
                rs[i].valid = true;
                rs[i].dest_reg = inst.dest_reg;

                rs[i].fu = inst.op_code;

                rs[i].inst_num = inst.inst_num;

                if(inst.src_reg[0] == -1) rs[i].src_ready[0] = true;
                else rs[i].src_ready[0] = rf[inst.src_reg[0]].ready;

                if(inst.src_reg[1] == -1) rs[i].src_ready[1] = true;
                else rs[i].src_ready[1] = rf[inst.src_reg[1]].ready;

                if(!rs[i].src_ready[0]) rs[i].src_tag[0] = rf[inst.src_reg[0]].tag;
                if(!rs[i].src_ready[1]) rs[i].src_tag[1] = rf[inst.src_reg[1]].tag;

                rs[i].dest_tag = inst.inst_num;
                if(rs[i].dest_reg != -1) {
                    rf[rs[i].dest_reg].tag = rs[i].dest_tag;
                    rf[rs[i].dest_reg].ready = false;
                }
                //Stat: schedule cycle
                op[rs[i].inst_num].schedule_cycle = cycles+1;
                if(debug) {
                    cout << cycles << " Scheduled: " << rs[i].inst_num+1 << "\t SQ " << i << endl;
                }

            }
        }

        if(debug) {
            for(int i=0;i<rs_size;i++) {
                cout << "\t" <<i << ' ' << rs[i].inst_num+1 << '\t' << rs[i].valid << endl;
            }
        }
        /**Fetch: Populate dispq*/

        fetch(debug);
        int dq_size = disp_q.size();
        if(dq_size > max_dq_size) { max_dq_size = dq_size; }
        total_dq_size += dq_size;

        if(debug) {
            int temp_dqSize = disp_q.size();
            for(int i=0;i<temp_dqSize;i++) {
                proc_inst_t temp = disp_q.front();
                cout << '\t' << temp.inst_num << endl;
                disp_q.pop();
                disp_q.push(temp);
            }
        }


    //}while(cycles<25);
    }while(total_fetched != total_finished); //do while fetched_instructions != state-updated-instructions
}



/**
 * Subroutine for cleaning up any outstanding instructions and calculating overall statistics
 * such as average IPC, average fire rate etc.
 * XXX: You're responsible for completing this routine
 *
 * @p_stats Pointer to the statistics structure
 */
void complete_proc(proc_stats_t *p_stats) {
    p_stats->avg_inst_retired = (double) total_finished/(cycles+1);
    p_stats->avg_inst_fired = (double)total_fired/(cycles+1);
    p_stats->avg_disp_size = (double)total_dq_size/(cycles+1);
    p_stats->max_disp_size = max_dq_size;
    p_stats->retired_instruction = inst_num;
    p_stats->cycle_count = cycles+1;
}

void print_output() {
    cout << "INST\tFETCH\tDISP\tSCHED\tEXEC\tSTATE"<<endl;
    for(int i=0;i<100000;i++) {
    //for(int i=0;i<30;i++) {
        cout << dec << i+1 <<'\t' <<op[i].fetch_cycle<<'\t'<<op[i].dispatch_cycle<<'\t'<<op[i].schedule_cycle<<'\t'<<op[i].execute_cycle<<'\t'<<op[i].state_update_cycle<<endl;
    }
    //cout << total_fetched << '\t' << total_finished;
}
